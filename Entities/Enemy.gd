extends KinematicBody2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"
class_name Enemy

export (bool) var follow_player = true
export (float) var speed = 100
export (float) var contamination_factor = 1.6

var direction = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	add_to_group("Enemies")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	pass

func _physics_process(delta: float) -> void:
	if follow_player:
		var players = get_tree().get_nodes_in_group("Players")
		if players.size() > 0:
			var player = players[0]
			direction = player.position - self.position
			direction = direction.normalized() 
			move_and_slide(direction * speed)
