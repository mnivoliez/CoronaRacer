extends Node2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

export (String, FILE) var enemy_scene_path
export (float) var spawn_frequency = 5 

var enemy_scene
var spawn_timeout = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	enemy_scene = load(enemy_scene_path)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if spawn_timeout > spawn_frequency:
		var enemy_instance = enemy_scene.instance()
#		var random_x = rand_range(-100, 100)
#		var random_y = rand_range(-100, 100)
		get_tree().get_current_scene().call_deferred("add_child", enemy_instance)
		spawn_timeout = 0
	else:
		spawn_timeout += delta
