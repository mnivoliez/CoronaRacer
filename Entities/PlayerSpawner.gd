extends Node2D


export (String, FILE) var player_scene
# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if player_scene != null:
		var player_instance = load(player_scene).instance()
		player_instance.position = self.position
		get_tree().get_current_scene().call_deferred("add_child", player_instance)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
