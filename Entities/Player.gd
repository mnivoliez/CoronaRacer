extends KinematicBody2D

class_name Player

export (int) var speed = 110
export (float) var contamination_level = 0.0
export (float) var contamination_limit = 250

var velocity = Vector2()
var input_direction = Vector2()
var contation_per_second = 0.0

func _ready():
	add_to_group("Players")
	
func process_axis():
	if Input.is_action_pressed('right'):
		input_direction.x = 1
	elif Input.is_action_pressed('left'):
		input_direction.x = -1
	else:
		input_direction.x = 0
		
	if Input.is_action_pressed('down'):
		input_direction.y = 1
	elif Input.is_action_pressed('up'):
		input_direction.y = -1
	else:
		input_direction.y = 0
		
func get_input():
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	process_axis()
	
	if Input.is_action_pressed('scream'):
		print("Scream")

func _process(delta: float) -> void:
	if input_direction == Vector2():
		play_animation("idle")
	else: 
		if abs(input_direction.y) > abs(input_direction.x):
			if input_direction.y > 0:
				play_animation("walking_down")
			else:
				play_animation("walking_up")
	contamination_level += contation_per_second * delta
	if contamination_level > contamination_limit:
		get_node("/root/Globals").loose()
#	else:
#		print("Contamination level: ", contamination_level)
	
func _physics_process(delta):
	get_input()
	
	# Calculate velocity from input
	velocity = input_direction.normalized() * speed
	move_and_slide(velocity)

func play_animation(animation):
	var current_anim = $AnimatedSprite.get_animation()
	if current_anim != animation:
		$AnimatedSprite.play(animation)
		print($AnimatedSprite.animation)

func _on_ContaminationTriggerArea_body_entered(body: Node) -> void:
	if body.get_script() == Enemy:
		var enemy = body as Enemy
		contation_per_second += enemy.contamination_factor
		
func _on_ContaminationTriggerArea_body_exited(body: Node) -> void:
	if body.get_script() == Enemy:
		var enemy = body as Enemy
		contation_per_second -= enemy.contamination_factor
