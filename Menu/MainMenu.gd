extends Control


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_StartButton_pressed() -> void:
	get_node("/root/Globals").play_next_level()

func _on_QuitButton_pressed() -> void:
	get_node("/root/Globals").quit_game()
