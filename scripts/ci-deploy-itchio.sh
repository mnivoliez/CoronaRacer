# upload to itch.io 
export TOOLS_DIR=$PWD/tools
mkdir -p ${TOOLS_DIR}
cd ${TOOLS_DIR}
curl -sLo butler.zip "https://broth.itch.ovh/butler/linux-amd64-head/LATEST/.zip"
unzip butler.zip
cd ..

${TOOLS_DIR}/butler -V

${TOOLS_DIR}/butler push build/linux mnivoliez/corona-racer:linux-64
${TOOLS_DIR}/butler push build/windows mnivoliez/corona-racer:windows-64
${TOOLS_DIR}/butler push build/mac mnivoliez/corona-racer:mac-osx
${TOOLS_DIR}/butler push build/web mnivoliez/corona-racer:html5
