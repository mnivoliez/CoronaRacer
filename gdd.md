# Corona Racer

## Overview
Le jeu est un runner 2D vue du dessus. Le joueur doit aller depuis un point à un point B en evitant les personnes ne respectant pas les gestes barrières (IA).

## Mechaniques

Le joueur peut:
- courir en x et y et diagonale
- repousser aggressivement les autres personnages qui sont devant lui

Le joueur doit
- eviter les autres personnages dans un rayon de 2m
- éviter que sa barre de contamination ne deviennent pleine

Victoire: s'il atteint l'objectif
Défaite: si sa barre devient pleine

## Asset

Graphique: 
- Sprite du joueur: 
  - Immobile
  - Course (haut bas gauche droite)
  - Cri
- UI
  - Barre de contamination
  - Widget cri (avec cooldown)
  - Boussole
- sprite des ennemies:
  - Course (haut bas gauche droite)
- sprite decor
  - route
  - herbe
  - arbre
  - buisson
  - batîment
- Menu principal
  - Background
  - Button commencer
  - Button quitter
- Menu pause
  - Fond semi opaque
  - Button Reprendre
  - Button Quitter
- Menu Gameover
  - Message Gameover
  - Boutton quitter
  - Boutton Recommencer
- Menu Win
  - Message Win
  - Boutton Niveau suivant
  - Boutton Recommencer

Son:
- Bruit de pas
- Cri
- Musique de fond
- Bruit succès
- Bruit ennemie
- Gameover
- Contamination

Programmation: 
- UI
  - Boussole (direction + distance)
- Menu principal
- Menu Pause
- Menu Gameover
- Menu Win
- IA ennemie
  - Solo
  - Group (Boid)
  - Contamine le joueur
- Stats du joueur
  - niveau de contamination
  - cooldown du cri
- Action
  - Courrir
  - Cri
- GameState

## Tech

- Godot
- Photoshop
- Audacity (maybe)
- Banque de son opensource
- Git + LFS

## Workflow

- Integration continue
- Deploiment continue sur itch.io 

## Licence

MIT
