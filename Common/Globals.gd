extends Node

# ------------------------------------
# All the GUI/UI-related variables

var canvas_layer = null

#const DEBUG_DISPLAY_SCENE = preload("res://Menus/Debug_Display.tscn")
var debug_display = null

const MAIN_MENU_PATH = "res://Menu/MainMenu.tscn"
const POPUP_SCENE = preload("res://Menu/PauseMenu.tscn")
const POPUP_WIN = preload("res://Menu/WinMenu.tscn")
const POPUP_GAMEOVER = preload("res://Menu/GameOverMenu.tscn")

const LEVELS = ["res://Level/Level_1.tscn"]

var next_level = 0

var popup = null
var button_group = []

var has_win = false
var has_loose = false
# ------------------------------------

# ------------------------------------

func _ready():
	canvas_layer = CanvasLayer.new()
	add_child(canvas_layer)
	

func _process(_delta):	
	if has_win:
		show_win_menu()
		
	if has_loose:
		show_loose_menu()
		
	if Input.is_action_just_pressed("pause"):
		show_pause_menu()
	

func show_win_menu():
	has_win = false
	popup = POPUP_WIN.instance()

	popup.get_node("Button_quit").connect("pressed", self, "popup_quit")
	popup.connect("popup_hide", self, "popup_closed")
	popup.get_node("Button_retry").connect("pressed", self, "play_again")
	popup.get_node("Button_next").connect("pressed", self, "play_next_level")

	canvas_layer.add_child(popup)
	popup.popup_centered()

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	get_tree().paused = true
	
func show_loose_menu():
	has_loose = false
	popup = POPUP_GAMEOVER.instance()
	popup.get_node("Button_quit").connect("pressed", self, "popup_quit")
	popup.connect("popup_hide", self, "popup_closed")
	popup.get_node("Button_retry").connect("pressed", self, "on_play_again")

	canvas_layer.add_child(popup)
	popup.popup_centered()

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	get_tree().paused = true
	
func show_pause_menu():
	if popup == null:
		popup = POPUP_SCENE.instance()
		
		popup.get_node("Button_quit").connect("pressed", self, "popup_quit")
		popup.connect("popup_hide", self, "popup_closed")
		popup.get_node("Button_resume").connect("pressed", self, "popup_closed")
		popup.get_node("Button_retry").connect("pressed", self, "on_play_again")

		button_group = [
			popup.get_node("Button_quit"),
			popup.get_node("Button_resume"),
			popup.get_node("Button_retry")
		]
		canvas_layer.add_child(popup)
		popup.popup_centered()

		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		popup.get_node("Button_quit").grab_focus()

		get_tree().paused = true
	
func load_new_scene(new_scene_path):	
	if get_tree().change_scene(new_scene_path) == OK:
		print("Scene", new_scene_path, "loaded")
	else:
		print("Can't load scene", new_scene_path)
	
func win():
	has_win = true
	
func loose():
	has_loose = true
	
func play_again():
	get_tree().paused = false
	
	if get_tree().reload_current_scene() == OK:
		print("Scene reloaded")
	else:
		print("Error: can't reload scene")
	popup_closed()

func play_next_level():
	get_tree().paused = false
	
	if next_level < LEVELS.size():
		load_new_scene(LEVELS[next_level])
		next_level += 1
	else:
		next_level = 0
		load_new_scene(MAIN_MENU_PATH)
	popup_closed()

func popup_closed():
	print("Popup closing")
	get_tree().paused = false

	if popup != null:
		popup.queue_free()
		popup = null
	button_group = []

func popup_quit():
	print("Popup request to return to main menu")
	get_tree().paused = false

	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	if popup != null:
		popup.queue_free()
		popup = null

	load_new_scene(MAIN_MENU_PATH)

func quit_game():
	get_tree().quit(0)
